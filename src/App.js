import React from 'react'
import './App.css';
import {Exclamation, ArrowRightShort, ArrowRepeat, Pencil, Person, Stopwatch} from "react-bootstrap-icons";
import Highlighter from "react-highlight-words";
var dateFormat = require('dateformat');
var FileSaver = require('file-saver');

//import Bitrix from '@2bad/bitrix'

var MISSED_DEADLINE_TEXT = 'Завершите задачу или передвиньте срок'

var TAG_AUTOWATCH = 'autowatch'
var TAG_WATCHED = 'watched'
var TAG_LATER = 'later'
var TAG_ATTENTION = 'attention'
var STATE_COMPLETED = '5'

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}


class Settings {
  MY_ID = ''
  BITRIX_KEY = ''
  constructor() {
    var data =  localStorage.getItem('settings');
    if (data) {
      data = JSON.parse(data)
      this.MY_ID = data.MY_ID
      this.BITRIX_KEY = data.BITRIX_KEY
    }
  }
  preserve() {
    var data = {
      MY_ID: this.MY_ID,
      BITRIX_KEY: this.BITRIX_KEY
    }
    localStorage.setItem('settings', JSON.stringify(data));
  }
  initialized() {
    if (this.MY_ID && this.BITRIX_KEY)
      return true
    return false
  }
}

var settings = new Settings()

class BitrixApi {

  async perform_request(method, params) {
      var base_url = `https://bitrixproxy.awkward.me/${settings.BITRIX_KEY}/`
    return await fetch(base_url + method, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(params)})
      .then(res => res.json())
      .then(res => res['result'])
  }

  encodeGetParams(params) {
    return Object.entries(params)
      .map(kv => kv.map(encodeURIComponent)
           .join("=")).join("&")
  }

  // params: [{method:, params:, callback:}]
  async perform_batch_request(param_callback_array){
    if (param_callback_array.length === 0)
      return
    var request_dict = {}
    var callback_dict = {}
    var response_dict = {}
    for (var index = 0; index < param_callback_array.length; ++index) {
      var item = param_callback_array[index];
      var q = item.method + '?' + this.encodeGetParams(item.params)
      request_dict[index.toString()] = q
      callback_dict[index.toString()] = item.callback
      if (Object.keys(request_dict).length === 50 || index === param_callback_array.length - 1) {
        if (Object.keys(response_dict).length > 0) {
          await sleep(500);
        }
        var res = await this.perform_request('batch', {cmd: request_dict})
        res = res['result']
        response_dict = Object.assign({}, response_dict, res)
        request_dict = {}
      }
    }
    for (var [id, result] of Object.entries(response_dict)) {
      callback_dict[id](result)
    }
  }
}

var bitrixApi = new BitrixApi()


class TaskStore {
  all_tasks = {}
  constructor() {
    const dbconnect = window.indexedDB.open('task_db', 1);
    dbconnect.onupgradeneeded = ev => {
      const db = ev.target.result;
      db.createObjectStore('task', { keyPath: 'ID' });
    }
    dbconnect.onsuccess = async ev => {
      const db = ev.target.result;
      const store = db.transaction('task', 'readonly').objectStore('task');
      const query = store.openCursor()
      query.onerror = ev => {
        console.error('Request failed!', ev.target.error.message);
      };
      var tasks_2 = {}
      query.onsuccess = ev => {
        const cursor = ev.target.result;
        if (cursor) {
          tasks_2[cursor.value.ID] = cursor.value
          cursor.continue();
        }
      };
      await query
      this.all_tasks = tasks_2
      db.close()
    }
    this.init_finished = dbconnect.onsuccess
  }

  preserve(ID = undefined) {
    const dbconnect = window.indexedDB.open('task_db', 1);
    dbconnect.onsuccess = ev => {
      const db = ev.target.result;
      const transaction = db.transaction('task', 'readwrite');
      const store = transaction.objectStore('task');
      if (ID) {
        store.put(this.all_tasks[ID])
      } else {
        Object.values(this.all_tasks).map(task => store.put(task));
      }
      transaction.onerror = ev => {
        console.error('An error has occured!', ev.target.error.message);
      };
    }
    dbconnect.onerror = ev => {
      console.error('An error has occured!', ev.target.error.message);
    }
  }

  prepare_comment_call(task) {
    var call_data = {
      method: "task.commentitem.getlist",
      params: {TASKID: task.ID}
    }
    call_data.callback = res => {
      var last_old_id = 0
      if (task.comments && task.comments.length)
        last_old_id = task.comments[task.comments.length - 1].ID
      task.comments = res.slice(-10);
      if (task.comments.length === 0)
        return
      var last_comment = task.comments[task.comments.length - 1];
      if (last_comment.AUTHOR_ID === settings.MY_ID.toString()
          && last_comment.POST_MESSAGE.search(MISSED_DEADLINE_TEXT) !== 0) {
        task.watched = true
        return
      }
      if (task.watched && task.autowatch) {
        for(var i = 0; i < task.comments.length; i++) {
          var comment = task.comments[i]
          if (comment.id <= last_old_id)
            continue
          if (comment.POST_MESSAGE.search(MISSED_DEADLINE_TEXT) === 0 &&
            comment.POST_MESSAGE.search("USER="+settings.MY_ID) > 0) {
            task.watched = false
          }
        }
      }
    }
    return call_data
  }

  prepare_extra_data_call(task) {
    var call_data = {
      method: "task.item.getdata",
      params: {TASKID: task.ID}
    }
    call_data.callback = res => {
      task.complexity = parseFloat(res.UF_AUTO_108411695233)
      task.importance = parseFloat(res.UF_AUTO_238160223371)
      task.urgency = parseFloat(res.UF_AUTO_853527648145)
    }
    return call_data
  }

  async add_extra_data(tasks) {
    var batch_request = []
    tasks.forEach(task => {
      batch_request.push(this.prepare_comment_call(task))
      batch_request.push(this.prepare_extra_data_call(task))
    })
    await bitrixApi.perform_batch_request(batch_request)
  }
  async add_new_tasks(new_tasks) {
    var preserve_fields = [
      "ACTIVITY_DATE",
      "CHANGED_BY",
      "CHANGED_DATE",
      //"CLOSED_BY",
      //"CLOSED_DATE",
      "COMMENTS_COUNT",
      "CREATED_BY",
      "CREATED_BY_LAST_NAME",
      //"CREATED_BY_NAME",
      //"CREATED_BY_SECOND_NAME",
      "CREATED_DATE",
      "DATE_START",
      "DEADLINE",
      //"DECLINE_REASON",
      "DESCRIPTION",
      "DURATION_FACT",
      "DURATION_PLAN",
      "DURATION_TYPE",
      "END_DATE_PLAN",
      "FAVORITE",
      //"FORKED_BY_TEMPLATE_ID",
      "GROUP_ID",
      //"GUID",
      "ID",
      //"MATCH_WORK_TIME",
      //"PARENT_ID",
      "PRIORITY",
      "REAL_STATUS",
      "RESPONSIBLE_ID",
      "RESPONSIBLE_LAST_NAME",
      //"RESPONSIBLE_NAME",
      //"RESPONSIBLE_SECOND_NAME",
      "STAGE_ID",
      "START_DATE_PLAN",
      //"STATUS",
      //"STATUS_CHANGED_BY",
      //"STATUS_CHANGED_DATE",
      //"SUBORDINATE",
      //"TASK_CONTROL",
      "TIME_ESTIMATE",
      "TIME_SPENT_IN_LOGS",
      "TITLE",
    ]

    var tasks_to_extend = []
    for (var [ID, task_orig] of Object.entries(new_tasks)) {
      var task = {}
      for(var i = 0; i < preserve_fields.length; i++) {
        var field = preserve_fields[i];
        task[field] = task_orig[field];
      }
      var old = this.all_tasks[ID];
      if (old) {
        if (task.ACTIVITY_DATE === old.ACTIVITY_DATE)
          continue;
        task[TAG_AUTOWATCH] = old[TAG_AUTOWATCH]
        task[TAG_LATER] = old[TAG_LATER]
        task[TAG_ATTENTION] = old[TAG_ATTENTION]
        this.all_tasks[task.ID] = task;
        if (!task.autowatch)
          task.watched = false
        else
          task.watched = old.watched
      } else {
        this.all_tasks[task.ID] = task;
        task.watched = false
      }
      tasks_to_extend.push(task);
    }
    await this.add_extra_data(tasks_to_extend)
    this.preserve();
  }
  async update() {
    if (settings.initialized() === false)
      return
    await this.init_finished
    var filters = [
      {RESPONSIBLE_ID: settings.MY_ID},
      {AUDITOR: settings.MY_ID},
      {ACCOMPLICE: settings.MY_ID},
      {CREATED_BY: settings.MY_ID}
    ]
    var pages = Object.keys(this.all_tasks).length === 0 ? 3 : 1;
    var results = {}
    for (var index = 0; index < filters.length; ++index) {
      var filter = filters[index];
      for (var page = 1; page <= pages; ++page) {
        var res = await bitrixApi.perform_request("task.item.list", {
           "ORDER": {"ACTIVITY_DATE": "DESC"},
           "FILTER": filter,
           "PARAMS": {"NAV_PARAMS": {"nPageSize": 50, "iNumPage": page}}, "SELECT": []}
        )
        for (let i = 0; i < res.length; i++) {
          var task = res[i];
          results[task.ID] = task;
        }
      }
    }
    await this.add_new_tasks(results)
  }
}

var taskStore = new TaskStore();


class Task extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      task: props.task,
      expand_comments: false,
    };
  }

  get_comments() {
    var comments = this.state.task.comments;
    comments = comments ? comments : [];
    if (!this.state.expand_comments) {
      comments = comments.slice(-3);
    }
    return comments;
  }

  form_changed(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    var task = this.state.task;
    task[name] = value;
    var orig_task = taskStore.all_tasks[task.ID];
    orig_task[name] = value;
    taskStore.preserve(task.ID);
    this.forceUpdate();
  }

  render_comment(comment) {
    var time_till_now = new Date() - Date.parse(comment.POST_DATE)
    time_till_now = time_till_now / 1000 / 60;
    if (time_till_now > 24 * 7 * 60) {
      time_till_now = dateFormat(comment.POST_DATE, "yyyy.mm.dd, HH:MM")
    } else {
      var days = Math.floor(time_till_now / 24 / 60);
      time_till_now = time_till_now - days * 24 * 60;
      var hours = Math.floor(time_till_now / 60);
      time_till_now = time_till_now - hours * 60;
      var minutes = Math.floor(time_till_now);
      time_till_now = `${days}д ${hours}ч ${minutes}м`
    }
    var message = comment.POST_MESSAGE;
    if (message.search(MISSED_DEADLINE_TEXT) > 0)
      message = 'Просрачивается'
    var my_comment = comment.AUTHOR_ID === settings.MY_ID.toString()
    return (
      <tr key={comment.ID} className={`${my_comment ? 'text-muted' : ''}`}>
        <td className='small' style={{width: '20%', padding: 4}}>
          <div className="wrapper">
            <div><Stopwatch/> {time_till_now} </div>
            <div><Person/> {comment.AUTHOR_NAME}</div>
          </div>
          </td>
        <td className='small' style={{padding: 4}}>
          <Highlighter
            highlightClassName="strong"
            searchWords={["USER="+settings.MY_ID]}
            autoEscape={false}
            textToHighlight={message}/>
        </td>
      </tr>);
  }

  render() {
    var task = this.state.task;
    return (
      <div className={`card bg-light m-2 ${this.state.task.watched ? "text-muted" : ""}`}>
        <div className="card-header p-1">
          <div className="row">
            <div className="col-4"> #{this.state.task.ID}
              <strong className="text-gray-dark ml-1">
                {this.state.task.CREATED_BY_LAST_NAME}
                <ArrowRightShort/>
                {this.state.task.RESPONSIBLE_LAST_NAME}
              </strong>
            </div>
            <div className="col-3">
              <Pencil /> {dateFormat(this.state.task.ACTIVITY_DATE, "yyyy.mm.dd, HH:MM")}
            </div>
            <div className="col-2">
              <Stopwatch /> {this.state.task.DEADLINE ? dateFormat(this.state.task.DEADLINE, "yyyy.mm.dd") : '-'}
            </div>
            <div className="col-3">
              Создана {dateFormat(this.state.task.CREATED_DATE, "yyyy.mm.dd")}
            </div>
          </div>
        </div>
        <div className="card-body p-2">
          <h5 className="card-title">
            <a href={`https://b24.ubirator.com/workgroups/group/${this.state.task.GROUP_ID}/tasks/task/view/${this.state.task.ID}/`}>
            <Exclamation/>{this.state.task.importance}
            <Stopwatch/>{this.state.task.urgency}
            {task.REAL_STATUS === STATE_COMPLETED ? <del>{task.TITLE}</del> : task.TITLE}
          </a></h5>
          <p className="card-text small">{this.state.task.DESCRIPTION}</p>
          <div className='row justify-content-start'>
            <div className='col-auto'>
            <form>
              <label className='bg-info rounded m-1'>
                Просмотрено
                <input name={TAG_WATCHED} type="checkbox"
                  checked={this.state.task[TAG_WATCHED]}
                  onChange={this.form_changed.bind(this)} />
              </label>
              <label className='bg-info rounded m-1'>
                Автопросмотр
                <input name={TAG_AUTOWATCH} type="checkbox"
                  checked={this.state.task[TAG_AUTOWATCH]}
                  onChange={this.form_changed.bind(this)} />
              </label>
              <label className='bg-info rounded m-1'>
                Отложено
                <input name={TAG_LATER} type="checkbox"
                  checked={this.state.task[TAG_LATER]}
                  onChange={this.form_changed.bind(this)} />
              </label>
              <label className='bg-info rounded m-1'>
                Важно
                <input name={TAG_ATTENTION} type="checkbox"
                  checked={this.state.task[TAG_ATTENTION]}
                  onChange={this.form_changed.bind(this)} />
              </label>
            </form>
            </div>
            <div className='col-auto'>
              <button type="button" className="btn btn-secondary btn-sm m-1 p-1" onClick={
                () => this.setState({expand_comments: !this.state.expand_comments})}>
                {this.state.expand_comments ? 'Свернуть' : 'Развернуть'}
              </button>
            </div>
          </div>
          <table className="table table-striped mb-0">
            <tbody>
              {this.get_comments().map(this.render_comment)}
            </tbody>
          </table>
        </div>
      </div>);
  }
}


class Updater extends React.Component {
  constructor(props) {
    super(props);
    var next_time = new Date();
    this.state = {
      update_callback: props.update_callback,
      next_time: next_time,
      time_left: ""
    };
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  update() {
    var next_time = new Date();
    next_time.setSeconds(next_time.getSeconds() + 300);
    this.setState({next_time: next_time});
    this.state.update_callback();
  }

  tick() {
    var time_left = Math.floor((this.state.next_time - new Date()) / 1000);
    if (time_left < 0) {
      this.update()
    }
    this.setState({time_left: time_left});
  }

  render() {
    return (
      <button type="button" className="btn btn-secondary btn-sm p-1"
        style={{width: '130px'}}
        onClick={this.update.bind(this)}>
        Update ({this.state.time_left}) <ArrowRepeat/>
      </button>
    );
  }
}

class SettingsView extends React.Component {
  handle_form_change(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    settings[name] = value
    this.forceUpdate()
  }

  save_settings(event) {
    settings.preserve()
    event.preventDefault();
  }

  save_db(tasks, filter) {
    var data = {
      settings: {
        MY_ID: settings.MY_ID,
        BITRIX_KEY: settings.BITRIX_KEY
      }
    }
    data.tasks = taskStore.all_tasks
    var blob = new Blob([JSON.stringify(data)], {type: "text/plain;charset=utf-8"});
    FileSaver.saveAs(blob, "ubirator_bitrix_db.json");
  }

  async restore_db(event) {
    var data = await event.target.files[0].text();
    data = JSON.parse(data);
    taskStore.all_tasks = data.tasks;
    taskStore.preserve()
    settings.MY_ID = data.settings.MY_ID
    settings.BITRIX_KEY = data.settings.BITRIX_KEY
    settings.preserve()
	};

  render() {
    return (
      <>
        <div className='row m-2 p-1'>
          <div className='col-6'>
            <p>
              Бекапирование и восстановление работы проделанной с задачами в этой системе
            </p>
          </div>
          <div className='col-3'>
            <button type="button" className="btn btn-secondary btn-sm m-1 p-1"
              onClick={this.save_db.bind(this)}>
            SaveDB
            </button>
          </div>
          <div className="col-3 custom-file">
            <input type="file" className="custom-file-input" name="file"
              onChange={this.restore_db.bind(this)} />
            <label className="custom-file-label" htmlFor="customFile">Restore DB</label>
          </div>
        </div>
        <div className='row m-2 p-1'>
          <div className='col-6'>
            <p>
              ID пользователя можно найте в url в битриксе на странице своего пользователя. Bitrix key необходимо получить у алексея
            </p>
          </div>
          <div className='col-auto'>
            <form onSubmit={this.save_settings.bind(this)}>
                <label className='bg-info rounded m-1'>
                   Bitrix user id
                 <input type="text" className="form-control"
                   value={settings.MY_ID}
                   name="MY_ID" onChange={this.handle_form_change.bind(this)}/>
                </label>
                <label className='bg-info rounded m-1'>
                   Bitrix key
                 <input type="text" className="form-control"
                   value={settings.BITRIX_KEY}
                   name="BITRIX_KEY" onChange={this.handle_form_change.bind(this)}/>
                </label>
                <button type="submit" className="btn btn-primary mb-2">
                  Сохранить
                </button>
              </form>
            </div>
          </div>
        </>
      );
    }
  }

class CalendatView extends React.Component {
  constructor(props) {
    super(props);
    this.events = []
  }

  componentDidMount() {
    this.update()
  }

  async update() {
    if (!settings.initialized())
      return
    var data = await bitrixApi.perform_request(
      'calendar.event.get.nearest', {
      'type': 'user',
      'ownerId': settings.MY_ID,
      'days': 10,
      'forCurrentUser': 'true',
      'detailUrl': `/company/personal/user/${settings.MY_ID}/calendar/`
      })
    this.events = data
    this.forceUpdate()
	};

  render() {
    return (
      <div className='row m-2 p-1'>
        {this.events.map(event => (
          <div className='col-auto m-1 rounded'
            key={event.ID + event.DT_FROM_TS}
            style={{backgroundColor: '#e8ebea'}}>
            <h6 className='m-0'>
            {dateFormat(
              parseInt(event.DT_FROM_TS)*1000 + 3600 * 2 * 1000,
              "yyyy.mm.dd, HH:MM")}
            </h6>
            <div className='small m-0 p-0'>
              <a href={'https://b24.ubirator.com' + event['~detailUrl']}>
                {event.NAME}
              </a>
            </div>
          </div>)
        )}
      </div>
      );
    }
  }

class Tasks extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      [TAG_WATCHED]: "false",
      [TAG_LATER]: "undefined",
      [TAG_ATTENTION]: "undefined",
      RESPONSIBLE: "",
      URGENCY: "",
    };
  }

  filter_changed(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    this.setState({[name]: value});
  }

  fetch_data() {
    taskStore.update()
      .then(() => this.forceUpdate())
  }

  apply_select_filter(tasks, filter) {
    var value = this.state[filter];
    if (value === 'undefined')
      return tasks;
    if (value === 'true')
      return tasks.filter(task => task[filter]);
    return tasks.filter(task => !task[filter]);
  }

  complete_unwatched() {
    Object.values(taskStore.all_tasks).forEach(task => {
      if (!task.watched && task.REAL_STATUS === STATE_COMPLETED)
        task.watched = true
    })
    taskStore.preserve()
    this.forceUpdate()
  }

  render() {
    var tasks = Object.values(taskStore.all_tasks);
    var non_watched_cnt = 0;
    var later_cnt = 0;
    var attention_cnt = 0;
    var completed_unwatched_cnt = 0;
    var responsible_persons = {}
    var urgency = {}
    for (var [id, task] of Object.entries(tasks)) {
      if (!task.watched) {
        non_watched_cnt += 1
        if (task.REAL_STATUS === STATE_COMPLETED)
          completed_unwatched_cnt += 1
      }
      if (task.REAL_STATUS === STATE_COMPLETED)
        continue
      if (task[TAG_LATER])
        later_cnt += 1
      if (task[TAG_ATTENTION])
        attention_cnt += 1
      var responsible = task.RESPONSIBLE_LAST_NAME
      if (responsible_persons[responsible])
        responsible_persons[responsible] += 1
      else
        responsible_persons[responsible] = 1
      if (task.urgency) {
        if (urgency[task.urgency])
          urgency[task.urgency] += 1
        else
          urgency[task.urgency] = 1
      }
    }
    responsible_persons = Object.entries(responsible_persons).map(rp => {
      var [name, cnt] = rp
      return {name: name, cnt: cnt}
    })
    responsible_persons.sort(function(a, b) {
      return a.name > b.name;
    });
    urgency = Object.entries(urgency).map(rp => {
      var [name, cnt] = rp
      return {name: name, cnt: cnt}
    })
    urgency.sort(function(a, b) {
      return a.name < b.name;
    });

    tasks = this.apply_select_filter(tasks, TAG_WATCHED)
    tasks = this.apply_select_filter(tasks, TAG_LATER)
    tasks = this.apply_select_filter(tasks, TAG_ATTENTION)
    if (this.state.RESPONSIBLE)
      tasks = tasks.filter(task => task.RESPONSIBLE_LAST_NAME === this.state.RESPONSIBLE);
    tasks.sort(function(a, b) {
      return a.ACTIVITY_DATE < b.ACTIVITY_DATE;
    });
    if (this.state.URGENCY)
      tasks = tasks.filter(task => task.urgency == this.state.URGENCY);
    tasks.sort(function(a, b) {
      return a.ACTIVITY_DATE < b.ACTIVITY_DATE;
    });
    tasks = tasks.slice(0, 60);
    return (
      <>
        <CalendatView/>
        <div className='row justify-content-between'>
          <div className='col-3'>
            <h4>Recently modified task</h4>
          </div>
          <div className='col-4'>
            <button type="button" className="btn btn-secondary btn-sm m-1 p-1"
              onClick={this.complete_unwatched.bind(this)}>
              Просмотреть завершенные ({completed_unwatched_cnt})
            </button>
          </div>
          <div className="col-2 custom-file small">
            <Updater update_callback={this.fetch_data.bind(this)}/>
          </div>
          <div className='col-auto'>
            <form>
                <label className='bg-info rounded m-1'>
                  Исполнитель
                  <select value={this.state.RESPONSIBLE}
                    name="RESPONSIBLE"
                    onChange={this.filter_changed.bind(this)}>
                    {responsible_persons.map(
                      rp =><option key={rp.name} value={rp.name}>{rp.name} ({rp.cnt})</option>)
                    }
                    <option value="">Любой</option>
                  </select>
                </label>
                <label className='bg-info rounded m-1'>
                  Срочность
                  <select value={this.state.URGENCY}
                    name="URGENCY"
                    onChange={this.filter_changed.bind(this)}>
                    {urgency.map(u =><option key={u.name} value={u.name}>{u.name} ({u.cnt})</option>)
                    }
                    <option value="">Любой</option>
                  </select>
                </label>
                <label className='bg-info rounded m-1'>
                  Отложено ({later_cnt})
                  <select value={this.state[TAG_LATER]}
                    name={TAG_LATER}
                    onChange={this.filter_changed.bind(this)}>
                    <option value="true">Да</option>
                    <option value="false">Нет</option>
                    <option value="undefined">Любое</option>
                  </select>
                </label>
                <label className='bg-info rounded m-1'>
                  Просмотрено (!{non_watched_cnt})
                  <select value={this.state[TAG_WATCHED]}
                    name={TAG_WATCHED}
                    onChange={this.filter_changed.bind(this)}>
                    <option value="true">Да</option>
                    <option value="false">Нет</option>
                    <option value="undefined">Любое</option>
                  </select>
                </label>
                <label className='bg-info rounded m-1'>
                  Важно ({attention_cnt})
                  <select value={this.state[TAG_ATTENTION]}
                    name={TAG_ATTENTION}
                    onChange={this.filter_changed.bind(this)}>
                    <option value="true">Да</option>
                    <option value="false">Нет</option>
                    <option value="undefined">Любое</option>
                  </select>
                </label>
              </form>
            </div>
          </div>
          {settings.initialized() ? 
            tasks.map((task) => (<Task key={task.ID} last_activity={task.ACTIVITY_DATE} task={task}/>))
            :
            <div>Укажите настройки подключения к битриксу</div>
          }
        </>
      );
    }
  }

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      show_settings: false
    };
  }

  render() {
    return (
      <>
      <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom box-shadow">
        <h5 className="my-0 mr-md-auto font-weight-normal">Bitrix parser</h5>
        <div className='col-auto'>
          <button type="button" className="btn btn-secondary btn-sm m-1 p-1" onClick={
            () => this.setState({show_settings: !this.state.show_settings})}>
            {this.state.show_settings ? 'Скрыть настройки' : 'Настройки'}
          </button>
        </div>
      </div>
      {this.state.show_settings ?
      <div className='container rounded' style={{'backgroundColor': '#e8ebea'}}>
        <SettingsView/>
      </div> : null
      }
      <main role="main" className="container">
        <Tasks/>
      </main>
      </>
    );
  }

}

export default App;
